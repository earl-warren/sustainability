# Project Plan:

This workplan contains tasks in the following scope:

* Tools to produce a multi-architecture Forgejo distribution

  The target architectures of Forgejo binaries are amd64, arm64, armv7 and armv6. Forgejo OCI compliant images are released for amd64 and arm64. Tools are developed to allow the creation of binaries and OCI images for all architectures on a single amd64 machine. They include testing of the distribution using the native architecture to verify it actually works as intended.

* An integrated release pipeline based on Forgejo

  A release pipeline based on continuous integration is developed to allow for building a Forgejo release for all targetted packages and architectures. The release manager pushes a tag to the continuous integration system hosted on a Forgejo instance to trigger the pipeline that:

  * Runs security checks
  * Runs unit and integration tests
  * Builds the OCI images
  * Builds the binaries
  * Uploads OCI images and binaries to a designated Forgejo instance

  the pipeline is also desginged to be able to copy releases from one Forgejo instance to another to allow for multi-stage releases for release candidates or experimental releases.

* A new continous integration agent

  A continous integration agent runs the workload for all continuous integration tasks, which includes the release pipeline itself. It is developed to be systemd capable, include nested hardware virtualization and nested system containers. The nested hardware virtualization allows integration test to be run even when they require KVM, for instance when testing the infrastructure as code to deploy Forgejo. The nested containers allows testing the continuous integration agent itself. The agent allows running workloads in LXC containers which are systemd capable to test the OCI images can effectively be used, for instance with database installed from packages native of a given distribution.

* Infrastructure as code to deploy Forgejo

  Infrastructure as code is developed (in shell and Ansible) to deploy Forgejo as well as the associated continuous integration agents. It includes tests that rely on Forgejo itself, recursively. It can then be used to deploy and upgrade Forgejo instances used by the forgejo project itself.

* Tooling and documentation to maintaing a soft fork

  As soft fork is a set of patches applied on top of an existing codebase. When the codebase changes, the same set of patches must be applied again on top of the new codebase. It may no longer work because the patch conflicts or because the features changed. As time passes and the number of patches grow, manually applying them again becomes increasingly difficult. Tools are developed to facilitate this work and best practices are documented to keep the maintenance burden to a minimum.

# Tasks

## Scope: Tools to produce a multi-architecture Forgejo distribution

### Tools to produce the binary Forgejo distribution

The Forgejo binary is built using a specific set of flags for the Go compiler. The resulting binary is tested to work using qemu architecture virtualization and binfmt.

Deliverable:
* URL to merged pull requests in https://codeberg.org/forgejo/forgejo

Milestone(s)
* Script to build binaries for amd64, arm64, armv6
* Support for GPG signing the binaries
* Tests for the script running the build
* CI configuration including the script
* Tests to verify the binary works on the target architecture

Amount:
    10%

### Tools to produce the OCI container Forgejo images

The Forgejo container image is built using buildx to support multiple platforms. The resulting image is tested to contain a usable binary image using the qemu architecture virtualization leveraged by docker and binfmt.

Deliverable:
* URL to merged pull requests in https://codeberg.org/forgejo/forgejo

Milestone(s)
* Script to build OCI images for amd64 and arm64
* Tests for the script running the build
* CI configuration including the script
* Tests to verify the image can run Forgejo on the target architecture

Amount
    10%

## Scope: An integrated release pipeline based on Forgejo

### Tag based release configuration

The release manager pushes a tag to the continuous integration system hosted on a Forgejo instance to trigger the pipeline.

Deliverable:

  * URL to merged pull requests in https://codeberg.org/forgejo/forgejo

Milestone(s):
  * Runs security checks
  * Runs unit and integration tests
  * Builds the OCI images
  * Builds the binaries
  * Uploads OCI images and binaries to a designated Forgejo instance

Amount
    2%

### DNS-Update-Checker RFC

Aprivacy-friendly and scalable system to check if an update is available (based on DNS)

Problems:
- the original (gitea) update-checker system leaked the IP-Address of the checking server
- the original update-checker relies on one https server (no trivial to scale)
- the Forgejo update-checker (based on DNS) started as an experiment, but could be standardized to benefit other projects

Deliverables:
- a RFC defining the goal and protocol details
- a client side implementation in Go, for integration in Forgejo

Amount: 5%
- RFC: 20 hours
- client implementation: 8 hours

### Cleaner Webhook system

Webhooks are the main way to communicate with other systems. Having a clean system will allow for other webhooks to be developed and Forgejo to better integrate in the wider forge ecosystem.

Problems: 
currently the webhook is a two-step process: a part of the payload is generated just after the webhook-event happened; the rest of the payload is defined just before send the actual request.
- this two-step process makes the current code very intricate and difficult to maintain
- it will help move forward the [Custom webhook pull request](https://github.com/go-gitea/gitea/pull/19307), since it will reduce its scope.

Deliverable:
- a pull request implementing the webhook system in one step (after the webhook-event)
- a pull request implementing a webook to https://builds.sr.ht/

Amount: 5%
- one step: 25 hours
- sourcehut webhook: 8 hours

## Scope: A new continous integration agent

### Forgejo runner

The Forgejo runner is at the heart of Forgejo and its release pipeline. The nested containers allows testing the continuous integration agent itself. The agent allows running workloads in LXC containers which are systemd capable to test the OCI images can effectively be used, for instance with database installed from packages native of a given distribution.

Deliverable:
  * URL to merged pull requests in https://code.forgejo.org/forgejo/runner
  * URL to merged pull requests in https://code.forgejo.org/actions/setup-forgejo
    
Millestone(s):
   * A binary that can be used as an agent for the Forgejo Actions CI
   * An Action that deploys Forgejo and the runner
   * Tests that verify the runner works in a nested system environment
    
Amount:
    20%

### LXC backend

The continuous integration agent is based on ACT which allows running workloads on a local machine. It is based on Docker and does not support nesting or systemd. An LXC backend is added to support both.

Deliverable:
   * URL to merged pull requests in https://code.forgejo.org/forgejo/act
    
Milestone(s):
   * Release that includes the LXC backend
   * Support for nested hardware virtualization (KVM)
   * Support for nested system containers

Amount:
    10%

## Scope: Infrastructure as code to deploy Forgejo

### Ansible based infrastructure as code

Real world Forgejo deployment require DNS, STMP relay, monitoring and more to provide a reliable service, all of which is available in Ansible roles and playbooks. Playbooks are developed to enable Forgejo (and the runner) in this environment. The playbooks are intended to be used to deploy and maintain Forgejo's own infrastructure in the spirit of dogfooding.

Deliverables:
   * URL to merged pull requests in https://code.forgejo.org/forgejo/infrastructure

Milestone(s):
   * Ansible role to deploy / upgrade Forgejo
   * Ansible role to deploy / upgrade the Forgejo runner
   * Ansible playbook to deploy / upgrade the Forgejo and the runner
   * Tests for the Ansible roles based on testinfra
   * Forgejo runner workflow to run the tests
    
Amount:
    20%

### Shell based infrastructure as code

Codeberg is deployed using shell based infrastructure as code. Shell scripts are written so they can be used by Codeberg in this context. The shell scripts are also meant to be a lightweight alternative to Ansible based infrastructure as code in the context of continuous integration, to deploy and run Forgejo and the associated runners.

Deliverables:
   * URL to merged pull requests in https://code.forgejo.org/actions/setup-forgejo

Milestone(s):
   * Script to launch Forgejo from docker
   * Obtain credentials for API and continuous integration agent access to the Forgejo instance
   * Script to launch the Forgejo runner from sources
   * Integration tests verifying the scripts work on a newly deployed Forgejo instance
     
Amount:
    10%
